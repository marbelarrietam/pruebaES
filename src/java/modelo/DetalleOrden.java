/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Marbel
 */
@Entity
@Table(name = "detalle_orden")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleOrden.findAll", query = "SELECT d FROM DetalleOrden d")
    , @NamedQuery(name = "DetalleOrden.findByIDdetalle", query = "SELECT d FROM DetalleOrden d WHERE d.iDdetalle = :iDdetalle")
    , @NamedQuery(name = "DetalleOrden.findByLargo", query = "SELECT d FROM DetalleOrden d WHERE d.largo = :largo")
    , @NamedQuery(name = "DetalleOrden.findByAncho", query = "SELECT d FROM DetalleOrden d WHERE d.ancho = :ancho")})
public class DetalleOrden implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_detalle")
    private Integer iDdetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "largo")
    private int largo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ancho")
    private int ancho;
    @JoinColumn(name = "orden", referencedColumnName = "nro_orden")
    @ManyToOne(optional = false)
    private Orden orden;

    public DetalleOrden() {
    }

    public DetalleOrden(Integer iDdetalle) {
        this.iDdetalle = iDdetalle;
    }

    public DetalleOrden(Integer iDdetalle, int largo, int ancho) {
        this.iDdetalle = iDdetalle;
        this.largo = largo;
        this.ancho = ancho;
    }

    public Integer getIDdetalle() {
        return iDdetalle;
    }

    public void setIDdetalle(Integer iDdetalle) {
        this.iDdetalle = iDdetalle;
    }

    public int getLargo() {
        return largo;
    }

    public void setLargo(int largo) {
        this.largo = largo;
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iDdetalle != null ? iDdetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleOrden)) {
            return false;
        }
        DetalleOrden other = (DetalleOrden) object;
        if ((this.iDdetalle == null && other.iDdetalle != null) || (this.iDdetalle != null && !this.iDdetalle.equals(other.iDdetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.DetalleOrden[ iDdetalle=" + iDdetalle + " ]";
    }
    
}
