package modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Cliente;
import modelo.DetalleOrden;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-09-20T12:14:48")
@StaticMetamodel(Orden.class)
public class Orden_ { 

    public static volatile SingularAttribute<Orden, Date> fecha;
    public static volatile SingularAttribute<Orden, Cliente> cliente;
    public static volatile SingularAttribute<Orden, String> estado;
    public static volatile CollectionAttribute<Orden, DetalleOrden> detalleOrdenCollection;
    public static volatile SingularAttribute<Orden, Integer> nroOrden;

}